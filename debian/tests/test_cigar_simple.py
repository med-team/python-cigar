from cigar import Cigar

def test_basic():
    #Test 100M
    c = Cigar('100M')
    assert len(c) == 100
    assert list(c.items()) == [(100, 'M')]

    # Test '20H20M20S'
    c = Cigar('20H20M20S')
    assert len(c) == 40
    assert str(c) == '20H20M20S'
    assert list(c.items()) == [(20, 'H'), (20, 'M'), (20, 'S')]
    assert c.mask_left(29).cigar, c.cigar == ('20H9S11M20S', '20H20M20S')

    # Test '10M20S10M'
    c = Cigar('10M20S10M')
    assert c.mask_left(10).cigar == '30S10M'
    assert c.mask_left(9).cigar == '9S1M20S10M'

    #Custom Tests
    assert [Cigar('10S').mask_left(10).cigar, Cigar('10H').mask_left(10).cigar, Cigar('10H').mask_left(11).cigar, Cigar('10H').mask_left(9).cigar]  == ['10S', '10H', '10H', '10H']
    assert Cigar('1M10H').mask_left(9).cigar, Cigar('5M10H').mask_left(9).cigar == ('1S10H', '5S10H')
    c = Cigar('1S1H1S5H1S5M10H')
    assert c.mask_left(9).cigar == c.cigar
    c = Cigar('1S1H1S5H1S5M10H')
    assert c.mask_right(9).cigar == c.cigar
    assert c.mask_right(11).cigar == '1S1H1S5H1S4M1S10H'
